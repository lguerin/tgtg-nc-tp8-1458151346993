import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'nc-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;
  email: FormControl;
  name: FormControl;
  password: FormControl;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initUserForm();
  }

  initUserForm(): void {
    // Init des champs
    this.email = this.fb.control('', [Validators.required, Validators.email]);
    this.name = this.fb.control('', [Validators.required]);
    this.password = this.fb.control('', [Validators.required, Validators.minLength(5)]);

    // Build du formulaire
    this.userForm = this.fb.group({
      email: this.email,
      name: this.name,
      password: this.password
    });
  }
}
